﻿
CREATE FUNCTION dbo.ufn_LooksLikePhoneNumber (@str nvarchar(500))
RETURNS bit
AS
BEGIN
	RETURN MDMTools.dbo.RegExpLike(RTRIM(ISNULL(@str,'')),'^[^0-9\(\+]{0,8}[0-9\s\-\(\)\+]{10,}',1)
END
