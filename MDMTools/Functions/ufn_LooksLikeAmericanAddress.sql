﻿
CREATE FUNCTION dbo.ufn_LooksLikeAmericanAddress (@str nvarchar(500))
RETURNS bit
AS
BEGIN
     RETURN MDMTools.dbo.RegExpLike(MDMTools.dbo.RegExpReplace(isnull(@str,''),'[\s]{1}([A-Z])\.([A-Z])\.[\s]{1}',' $1$2 ',0), '[a-zA-Z\s],[\s]+[a-zA-Z]{2}[\s]+[0-9\-]{5,10}',0)
END
