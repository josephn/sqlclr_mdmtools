﻿		create function dbo.ufn_ReturnGreater(@val1 int, @val2 int)
	returns int
	as
	begin
		IF ISNULL(ISNULL(@val1,@val2),0) > ISNULL(ISNULL(@val2,@val1),0)
			RETURN ISNULL(ISNULL(@val1,@val2),0)
		RETURN ISNULL(ISNULL(@val2,@val1),0)
	end