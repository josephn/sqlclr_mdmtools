﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace SharedETL
{
    /// <summary>
    /// Do not add anything here that can't be used in SQLCLR. This means C style procedural programming and external references.
    /// This class is for transforms, scrubbing and validations.
    /// </summary>
    public static class Transform
    {
        private readonly static Regex removeStartingThe;
        private readonly static Regex removeEndingInc;
        private readonly static Regex escapeSlash;
        private readonly static Regex normalizeString;
        private readonly static Regex removeInvalidCharacters;
        private readonly static Regex anyLetterAcronyms;
        private readonly static Regex removeSpaces;
        private readonly static Regex removeExtraSpaces;
        private const string escapeSlashReplace = @"$1\$2";
        private readonly static Regex removeAllButNumbers;

        static Transform()
        {
            anyLetterAcronyms = new Regex("((^|[\u0020])[a-zA-Z][\u0020])([a-zA-Z]([\u0020,]|$))+",
                RegexOptions.Compiled | RegexOptions.IgnoreCase, TimeSpan.FromSeconds(2));
            removeStartingThe = new Regex(@"^the\s", RegexOptions.Compiled | RegexOptions.IgnoreCase,
                TimeSpan.FromSeconds(2));
            removeEndingInc = new Regex(@"[\s,]+Inc[\.]{0,1}$", RegexOptions.Compiled | RegexOptions.IgnoreCase,
                TimeSpan.FromSeconds(2));
            escapeSlash = new Regex(@"(^|[^\\])([\/-])", RegexOptions.Compiled | RegexOptions.IgnoreCase,
                TimeSpan.FromSeconds(2));
            normalizeString = new Regex(@"[^\w\u0020\-]*", RegexOptions.Compiled | RegexOptions.IgnoreCase,
                TimeSpan.FromSeconds(2));
            removeInvalidCharacters = new Regex(@"[^\w\u0020-\u007E]*", RegexOptions.Compiled | RegexOptions.IgnoreCase,
                TimeSpan.FromSeconds(2));
            removeSpaces = new Regex(@"[\s]+", RegexOptions.Compiled, TimeSpan.FromSeconds(2));
            removeExtraSpaces = new Regex(@"[\s]{2,}", RegexOptions.Compiled, TimeSpan.FromSeconds(2));

            removeAllButNumbers = new Regex(@"[^0-9]", RegexOptions.Compiled, TimeSpan.FromSeconds(2));

        }

        /// <summary>
        /// Pass in "This is not I B M" and get true
        /// </summary>
        /// <param name="orgName"></param>
        /// <returns></returns>
        public static bool HasAcronyms(this string orgName)
        {
            if (String.IsNullOrWhiteSpace(orgName))
                return false;
            return anyLetterAcronyms.IsMatch(orgName);
        }

        public static string StripAllButNumbers(this string s)
        {
            if (String.IsNullOrEmpty(s))
                return s;
            return removeAllButNumbers.Replace(s, string.Empty);

        }

        /// <summary>
        /// Pass in "This is not I B M" and get "This is not IBM"
        /// </summary>
        /// <param name="orgName"></param>
        /// <returns></returns>
        public static string StripSpacesInAcronyms(this string orgName)
        {
            if (String.IsNullOrWhiteSpace(orgName))
                return orgName;
            foreach (Match s in anyLetterAcronyms.Matches(orgName))
            {
                orgName = orgName.Replace(s.Value.Trim(), removeSpaces.Replace(s.Value, ""));
            }
            return orgName;
        }


    


        /// <summary>
        /// Pass in "bét" and get "bet"
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static string RemoveDiacritics(this string str)
        {
            if (str == null) return null;
            var chars =
                from c in str.Normalize(NormalizationForm.FormD).ToCharArray()
                let uc = CharUnicodeInfo.GetUnicodeCategory(c)
                where uc != UnicodeCategory.NonSpacingMark
                select c;

            var cleanStr = new string(chars.ToArray()).Normalize(NormalizationForm.FormC);

            return cleanStr;
        }

        /// <summary>
        /// Pass in "Hello  World" and get "HelloWorld"
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public static string RemoveSpaces(this string s)
        {
            if (String.IsNullOrEmpty(s))
                return s;
            return removeSpaces.Replace(s, "");
        }

        /// <summary>
        /// Pass in "Hello   World" and get "Hello World"
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public static string ReduceSpacingGreaterThanOne(this string s)
        {
            if (String.IsNullOrEmpty(s))
                return s;
            return removeExtraSpaces.Replace(s, " ");
        }

        /// <summary>
        /// Pass in 1,0 and get 0
        /// </summary>
        /// <typeparam name="TNumber"></typeparam>
        /// <param name="number"></param>
        /// <param name="otherNumber"></param>
        /// <returns></returns>
        public static TNumber ReturnLesser<TNumber>(this TNumber number, TNumber otherNumber)
            where TNumber : struct, IComparable
        {
            if (number.CompareTo(otherNumber) <= 0)
                return number;
            return otherNumber;
        }

        /// <summary>
        /// Pass in 1,0 and get 1
        /// </summary>
        /// <typeparam name="TNumber"></typeparam>
        /// <param name="number"></param>
        /// <param name="otherNumber"></param>
        /// <returns></returns>
        internal static TNumber ReturnGreater<TNumber>(this TNumber number, TNumber otherNumber)
            where TNumber : struct, IComparable
        {
            if (number.CompareTo(otherNumber) >= 0)
                return number;
            return otherNumber;
        }

        /// <summary>
        /// Pass in "The Organization, Inc." and get "Organization"
        /// </summary>
        /// <param name="orgName"></param>
        /// <returns></returns>
        public static string RemoveIrrelevantWords(this string orgName)
        {
            if (String.IsNullOrWhiteSpace(orgName))
                return orgName;
            return removeEndingInc.Replace(removeStartingThe.Replace(orgName, ""), "");
        }

        public static string EscapeReservedCharacters(this string s)
        {
            if (String.IsNullOrWhiteSpace(s))
                return s;
            return escapeSlash.Replace(s, escapeSlashReplace);
        }

        public static string RemoveInvalidCharacters(this string s)
        {
            if (String.IsNullOrWhiteSpace(s))
                return s;
            return removeInvalidCharacters.Replace(s, "");
        }

        /// <summary>
        /// Removes invalid characters, replaces diacritics, removes prefix the and suffix inc, reduces extra spaces, lower case, shrink acronyms
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public static string NormalizeString(this string s)
        {
            if (String.IsNullOrWhiteSpace(s))
                return s;
            return normalizeString.Replace(s, "").RemoveDiacritics().RemoveIrrelevantWords().ReduceSpacingGreaterThanOne().ToLowerInvariant().StripSpacesInAcronyms();
        }

        public static string NullIf(this string s, string condition, StringComparison sc = StringComparison.InvariantCultureIgnoreCase)
        {
            if (String.IsNullOrWhiteSpace(s) || String.IsNullOrWhiteSpace(condition))
                return s;
            if (s.Equals(condition, sc))
                return null;
            return s;
        }


        /// <summary>
        /// Based on Kevin's ideas for a more standardized approach to comparing street addresses
        /// </summary>
        /// <param name="streetAddress"></param>
        /// <returns></returns>
        private static string adjustStreetAddressForMatching(string streetAddress)
        {
            streetAddress = replaceExpression(streetAddress, "\\bOne\\b", "1", true);
            streetAddress = replaceExpression(streetAddress, "\\bTwo\\b", "2", true);
            streetAddress = replaceExpression(streetAddress, "\\bThree\\b", "3", true);
            streetAddress = replaceExpression(streetAddress, "\\bFour\\b", "4", true);
            streetAddress = replaceExpression(streetAddress, "\\bFive\\b", "5", true);
            streetAddress = replaceExpression(streetAddress, "\\bSix\\b", "6", true);
            streetAddress = replaceExpression(streetAddress, "\\bSeven\\b", "7", true);
            streetAddress = replaceExpression(streetAddress, "\\bEight\\b", "8", true);
            streetAddress = replaceExpression(streetAddress, "\\bNine\\b", "9", true);


            streetAddress = replaceExpression(streetAddress, "\\bFirst\\b", "1st", true);
            streetAddress = replaceExpression(streetAddress, "\\bSecond\\b", "2nd", true);
            streetAddress = replaceExpression(streetAddress, "\\bThird\\b", "3rd", true);
            streetAddress = replaceExpression(streetAddress, "\\bFourth\\b", "4th", true);
            streetAddress = replaceExpression(streetAddress, "\\bFifth\\b", "5th", true);
            streetAddress = replaceExpression(streetAddress, "\\bSixth\\b", "6th", true);
            streetAddress = replaceExpression(streetAddress, "\\bSeventh\\b", "7th", true);
            streetAddress = replaceExpression(streetAddress, "\\bEighth\\b", "8th", true);
            streetAddress = replaceExpression(streetAddress, "\\bNinth\\b", "9th", true);

            streetAddress = replaceExpression(streetAddress, "\\bApartments\\b", "Apts", true);
            streetAddress = replaceExpression(streetAddress, "\\bApartment\\b", "Apt", true);
            streetAddress = replaceExpression(streetAddress, "\\bAvenue\\b", "Ave", true);
            streetAddress = replaceExpression(streetAddress, "\\bAvenues\\b", "Aves", true);
            streetAddress = replaceExpression(streetAddress, "\\bBroadway\\b", "Bdwy", true);
            streetAddress = replaceExpression(streetAddress, "\\bBoulevard\\b", "Blvd", true);

            streetAddress = replaceExpression(streetAddress, "\\bCenter\\b", "Ctr", true);
            streetAddress = replaceExpression(streetAddress, "\\bCircle\\b", "Cir", true);
            streetAddress = replaceExpression(streetAddress, "\\bCourt\\b", "Ct", true);
            streetAddress = replaceExpression(streetAddress, "\\bDepartment\\b", "Dept", true);
            streetAddress = replaceExpression(streetAddress, "\\bDrive\\b", "Dr", true);
            streetAddress = replaceExpression(streetAddress, "\\bFloor\\b", "Fl", true);
            streetAddress = replaceExpression(streetAddress, "\\bHighway\\b", "Hwy", true);
            streetAddress = replaceExpression(streetAddress, "\\bJunction\\b", "Jct", true);
            streetAddress = replaceExpression(streetAddress, "\\bLane\\b", "Ln", true);
            streetAddress = replaceExpression(streetAddress, "\\bLanes\\b", "Lns", true);
            streetAddress = replaceExpression(streetAddress, "\\bMount\\b", "Mt", true);
            streetAddress = replaceExpression(streetAddress, "\\bParkway\\b", "Pkwy", true);
            streetAddress = replaceExpression(streetAddress, "\\bPlace\\b", "Pl", true);
            streetAddress = replaceExpression(streetAddress, "\\bPlaza\\b", "Plz", true);
            streetAddress = replaceExpression(streetAddress, "\\bRoad\\b", "Rd", true);
            streetAddress = replaceExpression(streetAddress, "\\bRoads\\b", "Rds", true);
            streetAddress = replaceExpression(streetAddress, "\\bRoom\\b", "Rm", true);
            streetAddress = replaceExpression(streetAddress, "\\bRooms\\b", "Rms", true);
            streetAddress = replaceExpression(streetAddress, "\\bRoute\\b", "Rte", true);
            streetAddress = replaceExpression(streetAddress, "\\bSaint\\b", "St", true);
            streetAddress = replaceExpression(streetAddress, "\\bSuite\\b", "Ste", true);

            //Standardize additional street address info with #
            streetAddress = replaceExpression(streetAddress, "\\bApt\\b", "#", true);
            streetAddress = replaceExpression(streetAddress, "\\bUnit\\b", "#", true);
            streetAddress = replaceExpression(streetAddress, "\\bSte\\b", "#", true);

            streetAddress = replaceExpression(streetAddress, "\\bTerrace\\b", "Terr", true);
            streetAddress = replaceExpression(streetAddress, "\\bTurnpike\\b", "Tpke", true);
            

            streetAddress = replaceExpression(streetAddress, "\\bNorth\\b", "N", true);
            streetAddress = replaceExpression(streetAddress, "\\bNorthwest\\b", "NW", true);
            streetAddress = replaceExpression(streetAddress, "\\bNortheast\\b", "NE", true);
            streetAddress = replaceExpression(streetAddress, "\\bSouth\\b", "S", true);
            streetAddress = replaceExpression(streetAddress, "\\bSouthwest\\b", "SW", true);
            streetAddress = replaceExpression(streetAddress, "\\bSoutheast\\b", "SE", true);
            streetAddress = replaceExpression(streetAddress, "\\bEast\\b", "E", true);
            streetAddress = replaceExpression(streetAddress, "\\bWest\\b", "W", true);

            streetAddress = replaceExpression(streetAddress, "\\bStreet\\b", "St", true);


            return streetAddress;
        }

    

        private static string replaceExpression(string text, string input, string replaceText, bool ignoreCase)
        {
            RegexOptions regexOptions;

            if (ignoreCase)
                regexOptions = RegexOptions.IgnoreCase;
            else
                regexOptions = RegexOptions.None;
            
            return Regex.Replace(text, input, replaceText, regexOptions);
        }



    }

    public static class LevenshteinDistance
    {
        public static List<string> GetUnique(IEnumerable<string> inputElementList, double fuzzyness)
        {
            if (inputElementList == null)
                return new List<string>();
            List<string> elementList = inputElementList.ToList();

            for (int i = 0; i < elementList.Count - 1; i++)
            {
                for (int j = i + 1; j < elementList.Count;)
                {
                    int levenshteinDistance = Distance(elementList[i], elementList[j]);
                    int maxLength = Math.Max(elementList[i].Length, elementList[j].Length);
                    double score = 1.0 - (double)levenshteinDistance / maxLength;

                    if (score > fuzzyness)
                    {
                        elementList.RemoveAt(j);
                    }
                    else
                    {
                        j++;
                    }
                }
            }

            return elementList;
        }

        public static double DistanceScore(string src, string dest)
        {
            int levenshteinDistance = Distance(src, dest);

            if (src == null)
                src = String.Empty;

            if (dest == null)
                dest = String.Empty;

            int maxLength = Math.Max(src.Length, dest.Length);

            if (maxLength == 0)
                return 0;

            double score = 1.0 - (double)levenshteinDistance / maxLength;

            return score;
        }

        public static int Distance(string src, string dest)
        {
            if (String.IsNullOrWhiteSpace(src) && String.IsNullOrWhiteSpace(dest))
                return -1;

            if (src == null)
            {
                return dest.Length;
                //throw new ArgumentNullException("src");
            }

            if (dest == null)
            {
                return src.Length;
                //throw new ArgumentNullException("dest");
            }

            int[][] d = new int[src.Length + 1][];

            for (int x = 0; x < d.Length; x++)
            {
                d[x] = new int[dest.Length + 1];
            }

            int i, j, cost;
            char[] str1 = src.ToCharArray();
            char[] str2 = dest.ToCharArray();

            for (i = 0; i <= str1.Length; i++)
            {
                d[i][0] = i;
            }

            for (j = 0; j <= str2.Length; j++)
            {
                d[0][j] = j;
            }

            for (i = 1; i <= str1.Length; i++)
            {
                for (j = 1; j <= str2.Length; j++)
                {
                    if (str1[i - 1] == str2[j - 1])
                    {
                        cost = 0;
                    }
                    else
                    {
                        cost = 1;
                    }

                    d[i][j] = Math.Min(d[i - 1][j] + 1, Math.Min(d[i][j - 1] + 1, d[i - 1][j - 1] + cost));

                    if ((i > 1) && (j > 1) && (str1[i - 1] ==
                        str2[j - 2]) && (str1[i - 2] == str2[j - 1]))
                    {
                        d[i][j] = Math.Min(d[i][j], d[i - 2][j - 2] + cost);
                    }
                }
            }

            return d[str1.Length][str2.Length];
        }
    }

    public static class JaroWinkler
    {

        static JaroWinkler()
        {

        }
        /// <summary>
        /// Return Jaro Winkler score from comparing the two strings
        /// </summary>
        /// <param name="s1"></param>
        /// <param name="s2"></param>
        /// <returns></returns>
        public static double GetJaroWinklerScore(string s1, string s2)
        {
            s1 = s1 == null ? null : s1.ToLowerInvariant();
            s2 = s2 == null ? null : s2.ToLowerInvariant();

            var result =
            JaroWinklerDistance.proximity(s1, s2);



            return result;
        }

        public static class JaroWinklerDistance
        {
            /* The Winkler modification will not be applied unless the 
             * percent match was at or above the mWeightThreshold percent 
             * without the modification. 
             * Winkler's paper used a default value of 0.7
             */
            private static readonly double mWeightThreshold = 0.7;

            /* Size of the prefix to be concidered by the Winkler modification. 
             * Winkler's paper used a default value of 4
             */
            private static readonly int mNumChars = 4;


            /// <summary>
            /// Returns the Jaro-Winkler distance between the specified  
            /// strings. The distance is symmetric and will fall in the 
            /// range 0 (perfect match) to 1 (no match). 
            /// </summary>
            /// <param name="aString1">First String</param>
            /// <param name="aString2">Second String</param>
            /// <returns></returns>
            public static double distance(string aString1, string aString2)
            {
                return 1.0 - proximity(aString1, aString2);
            }


            /// <summary>
            /// Returns the Jaro-Winkler distance between the specified  
            /// strings. The distance is symmetric and will fall in the 
            /// range 0 (no match) to 1 (perfect match). 
            /// </summary>
            /// <param name="aString1">First String</param>
            /// <param name="aString2">Second String</param>
            /// <returns></returns>
            public static double proximity(string aString1, string aString2)
            {
                aString1 = aString1 ?? string.Empty;
                aString2 = aString2 ?? string.Empty;
                int lLen1 = aString1.Length;
                int lLen2 = aString2.Length;
                if (lLen1 == 0 || lLen2 == 0)
                    return 0.0;

                int lSearchRange = Math.Max(0, Math.Max(lLen1, lLen2) / 2 - 1);

                bool[] lMatched1 = new bool[lLen1];
                for (int i = 0; i < lMatched1.Length; i++)
                {
                    lMatched1[i] = false;
                }
                bool[] lMatched2 = new bool[lLen2];
                for (int i = 0; i < lMatched2.Length; i++)
                {
                    lMatched2[i] = false;
                }

                int lNumCommon = 0;
                for (int i = 0; i < lLen1; ++i)
                {
                    int lStart = Math.Max(0, i - lSearchRange);
                    int lEnd = Math.Min(i + lSearchRange + 1, lLen2);
                    for (int j = lStart; j < lEnd; ++j)
                    {
                        if (lMatched2[j]) continue;
                        if (aString1[i] != aString2[j])
                            continue;
                        lMatched1[i] = true;
                        lMatched2[j] = true;
                        ++lNumCommon;
                        break;
                    }
                }
                if (lNumCommon == 0) return 0.0;

                int lNumHalfTransposed = 0;
                int k = 0;
                for (int i = 0; i < lLen1; ++i)
                {
                    if (!lMatched1[i]) continue;
                    while (!lMatched2[k]) ++k;
                    if (aString1[i] != aString2[k])
                        ++lNumHalfTransposed;
                    ++k;
                }
                // System.Diagnostics.Debug.WriteLine("numHalfTransposed=" + numHalfTransposed);
                int lNumTransposed = lNumHalfTransposed / 2;

                // System.Diagnostics.Debug.WriteLine("numCommon=" + numCommon + " numTransposed=" + numTransposed);
                double lNumCommonD = lNumCommon;
                double lWeight = (lNumCommonD / lLen1
                                 + lNumCommonD / lLen2
                                 + (lNumCommon - lNumTransposed) / lNumCommonD) / 3.0;

                if (lWeight <= mWeightThreshold) return lWeight;
                int lMax = Math.Min(mNumChars, Math.Min(aString1.Length, aString2.Length));
                int lPos = 0;
                while (lPos < lMax && aString1[lPos] == aString2[lPos])
                    ++lPos;
                if (lPos == 0) return lWeight;
                return lWeight + 0.1 * lPos * (1.0 - lWeight);

            }


        }
    }
}
