﻿using System;
using System.Data.SqlTypes;
using System.Collections;
using System.Net;
using System.Net.Mime;
using System.Text;
using System.Text.RegularExpressions;
using Microsoft.SqlServer.Server;
using SharedETL;
using System.Collections.Generic;
using System.Collections.Concurrent;

public partial class SQLRegEx
{
    private class MatchResult
    {
        public SqlInt32 Index;
        public SqlInt32 Length;
        public SqlString Value;

        public MatchResult(SqlInt32 index, SqlInt32 length, SqlString value)
        {
            Index = index;
            Length = length;
            Value = value;
        }
    }

    

    [Flags]
    private enum ExactMatchColumnFlags
    {
        None = 0,
        Col1 = 1,
        Col2 = 2,
        Col3 = 4,
        Col4 = 8,
        Col5 = 16,
        Col6 = 32,
        Col7 = 64,
        Col8 = 128,
        Col9 = 256
    }

    [Flags]
    private enum TelephoneSegments
    {
        None = 0,
        AreaCode = 1,
        FirstThree = 2,
        LastFour = 4
    }

    [Flags]
    private enum DateSegments
    {
        None = 0,
        Day = 1,
        Month = 2,
        Year = 4
    }

    [Flags]
    private enum StreetAddressSegments
    {
        None = 0,
        HouseNumber = 1,
        StreetName = 2,
        SuiteAptNumber = 4,
        City = 8, // not used
        State = 16, // not used
        Zip = 32, // five digit zip - not used
        ZipExtended = 64, // nine digit zip code - not used
        Country = 128 
    }

    private class FeatureSetResult
    {
        public SqlString SourceId; // primary key for source row
        public SqlString TargetId; // priamry key for row being compared

        /// <summary>
        /// SQL Sprocs don't support binary bit operations on anything over 32 bit unless you are doing varbinary - I think
        /// </summary>
        public SqlInt32 ExactMatchColumns; // for the nine columns, bit set to true if exact match
        public SqlDouble? Name1_L; // Name Levenshtein score
        public SqlDouble? Name1_J; // Name Jaro-Winkler score
        public SqlDouble? Name2_L; // Name Levenshtein score
        public SqlDouble? Name2_J; // Name Jaro-Winkler score
        public SqlInt32? Date3_S; //segments of exact matches - year - month - day
        public SqlInt32? StreetAddress4_S; // segments of exact matches - beginning number exact match - middle text exact match - optional suite/apt number match
        public SqlDouble? StreetAddress4_L; // Levenshtein score for street address
        public SqlDouble? StreetAddress4_J; // jaro-winkler score for street address
        public SqlInt32? Telephone5_S; // segments of exact matches - area code - first three digits - last four digits
        public SqlDouble? Name6_L; // Name Levenshtein score
        public SqlDouble? Name6_J; // Name Jaro-Winkler score
        public SqlDouble? Name7_L; // Name Levenshtein score
        public SqlDouble? Name7_J; // Name Jaro-Winkler score
        public SqlDouble? Name8_L; // Name Levenshtein score
        public SqlDouble? Name8_J; // Name Jaro-Winkler score
        public SqlInt32? Telephone9_S; // segments of exact matches - area code - first three digits - last four digits


        public FeatureSetResult(SqlString sourceId, SqlString targetId, SqlInt32 exactMatchColumns, SqlDouble? name1_L, SqlDouble? name1_J, SqlDouble? name2_L, SqlDouble? name2_J, SqlInt32? date3_S, SqlInt32? streetAddress4_S, SqlDouble? streetAddress4_L, SqlDouble? streetAddress4_J, SqlInt32? telephone5_S, SqlDouble? name6_L, SqlDouble? name6_J, SqlDouble? name7_L, SqlDouble? name7_J, SqlDouble? name8_L, SqlDouble? name8_J, SqlInt32? telephone9_S)
        {
            SourceId = sourceId;
            TargetId = targetId;
            ExactMatchColumns = exactMatchColumns;
            Name1_L = name1_L;
            Name1_J = name1_J;
            Name2_L = name2_L;
            Name2_J = name2_J;
            Date3_S = date3_S;
            StreetAddress4_S = streetAddress4_S;
            StreetAddress4_L = streetAddress4_L;
            StreetAddress4_J = streetAddress4_J;
            Telephone5_S = telephone5_S;
            Name6_L = name6_L;
            Name6_J = name6_J;
            Name7_L = name7_L;
            Name7_J = name7_J;
            Name8_L = name8_L;
            Name8_J = name8_J;
            Telephone9_S = telephone9_S;
        }
    }

    [SqlFunction(FillRowMethodName = "FillFeatureSet", TableDefinition = "[SourceId] nvarchar(80), [TargetId] nvarchar(80), [ExactMatchColumns] int, [Name1_L] float, [Name1_J] float, [Name2_L] float, [Name2_J] float, [Date3_S] int, [StreetAddress4_S] int, [StreetAddress4_L] float, [StreetAddress4_J] float, [Telephone5_S] int, [Name6_L] float, [Name6_J] float, [Name7_L] float, [Name7_J] float, [Name8_L] float, [Name8_J] float, [Telephone9_S] int")]
    public static IEnumerable FeatureSetMatches(string sourceId, string targetId, string srcName1, string srcName2, DateTime? srcDate3, string srcStreetAddress4, string srcTelephone5, string srcName6, string srcName7, string srcName8, string srcTelephone9, string tgtName1, string tgtName2, DateTime? tgtDate3, string tgtStreetAddress4, string tgtTelephone5, string tgtName6, string tgtName7, string tgtName8, string tgtTelephone9)
    {
        ExactMatchColumnFlags exactMatches = ExactMatchColumnFlags.None;

        if ((srcName1 ?? string.Empty).Equals(tgtName1 ?? " ", StringComparison.CurrentCultureIgnoreCase))
            exactMatches = exactMatches | ExactMatchColumnFlags.Col1;
        if ((srcName2 ?? string.Empty).Equals(tgtName2 ?? " ", StringComparison.CurrentCultureIgnoreCase))
            exactMatches = exactMatches | ExactMatchColumnFlags.Col2;
        if ((srcDate3 ?? DateTime.MinValue).Equals(tgtDate3 ?? DateTime.MaxValue))
            exactMatches = exactMatches | ExactMatchColumnFlags.Col3;
        if ((srcStreetAddress4 ?? string.Empty).Equals(tgtStreetAddress4 ?? " ", StringComparison.CurrentCultureIgnoreCase))
            exactMatches = exactMatches | ExactMatchColumnFlags.Col4;
        if ((srcTelephone5 ?? string.Empty).eq)

        double? name1_L = (srcName1 == null || tgtName1 == null) ? (double?)null : LevenshteinDistance.DistanceScore(srcName1, tgtName1);
        double? name1_J = (srcName1 == null || tgtName1 == null) ? (double?)null : JaroWinkler.GetJaroWinklerScore(srcName1, tgtName1);
                        

        yield return new FeatureSetResult(sourceId, targetId, (int) exactMatches, name1_L, name1_J,null,null,null,null,null,null,null,null,null,null,null,null,null,null);
    }

    public static void FillFeatureSet(object obj, out SqlInt32 index, out SqlInt32 length, out SqlString value)
    {
        MatchResult match = (MatchResult)obj;
        index = match.Index;
        length = match.Length;
        value = match.Value;
    }

    private class HttpResult
    {
        public SqlInt32 HttpResponseCode;
        public SqlBytes ResponseBody;
        public SqlBytes ErrorBody;

        public HttpResult(SqlInt32 httpResponseCode, SqlBytes responseBody, SqlBytes errorBody)
        {
            HttpResponseCode = httpResponseCode;
            ResponseBody = responseBody;
            ErrorBody = errorBody;
        }
    }

    private class ReplaceResult
    {
        public SqlString Value;

        public ReplaceResult(SqlString value)
        {
            Value = value;
        }
    }

    [SqlFunction]
    public static bool RegExpLike(string text, string pattern, bool ignoreCase)
    {
        RegexOptions reg_ex_opt;

        if (ignoreCase == true)
        {
            reg_ex_opt = RegexOptions.IgnoreCase;
        }
        else
        {
            reg_ex_opt = RegexOptions.None;
        }

        Match match = Regex.Match(text, pattern, reg_ex_opt);
        return (match.Value != String.Empty);
    }

    [SqlFunction(IsDeterministic=true)]
    [return: SqlFacet(MaxSize = -1)]
    public static string RegExpReplace([SqlFacet(MaxSize = -1)]string text, string pattern, string replacement, bool ignoreCase)
    {
        string lookupKey = pattern + ignoreCase;
        if (compiledRegularExpressions.ContainsKey(lookupKey))
            return compiledRegularExpressions[lookupKey].Replace(text, replacement);
        else
        {
            RegexOptions reg_ex_opt;

            if (ignoreCase == true)
            {
                reg_ex_opt = RegexOptions.IgnoreCase;
            }
            else
            {
                reg_ex_opt = RegexOptions.None;
            }

            reg_ex_opt = reg_ex_opt | RegexOptions.Compiled;


            var regex = new Regex(pattern, reg_ex_opt);
            compiledRegularExpressions.TryAdd(lookupKey, regex);

            return regex.Replace(text, replacement);
            //var result = Regex.Replace(text, pattern, replacement, reg_ex_opt);
            //return result;
        }
        
    }


    [SqlFunction(IsDeterministic = true)]
    [return: SqlFacet(MaxSize = -1)]
    public static string ClearCache()
    {
        compiledRegularExpressions.Clear();
        return "Cleared";
    }


    static SQLRegEx()
    {
        RegexOptions reg_ex_opt = RegexOptions.Compiled | RegexOptions.CultureInvariant;
        normalizeSqlString = new Regex(@"[^\w\u0020\-]*", reg_ex_opt, TimeSpan.FromSeconds(2));
        escapeJavascriptString = new Regex(@"([\\""])", reg_ex_opt, TimeSpan.FromSeconds(2));
        removeInvalidCharacters = new Regex(@"[^\w\u0020-\u007E]*", reg_ex_opt, TimeSpan.FromSeconds(2));
    }

    private readonly static ConcurrentDictionary<string, Regex> compiledRegularExpressions = new ConcurrentDictionary<string, Regex>();

    private readonly static Regex normalizeSqlString;

    private readonly static Regex escapeJavascriptString;

    private readonly static Regex removeInvalidCharacters;

    [SqlFunction(IsDeterministic = true, IsPrecise = false)]
    [return: SqlFacet(MaxSize = -1)]
    public static string NormalizeFullTextSearchString([SqlFacet(MaxSize = -1)] string text)
    {
        string result = text;
        try
        {
            var parsedString = new SharedETL.FullTextSearch(text,FullTextSearchOptions.TrimPrefixAll).ToString();
            return parsedString;
        }
        catch
        {
            return text;
        }
    }

    //[SqlFunction(IsDeterministic = true,IsPrecise = false)]
    //[return: SqlFacet(MaxSize = -1)]
    //public static string RegExpShortName([SqlFacet(MaxSize = -1)]string text, bool removeSpaces)
    //{
        
    //    if (String.IsNullOrEmpty(text))
    //        return text;
    //    return text.ShortNameTransform(removeSpaces);
        
    //}

    [SqlFunction(IsDeterministic = false, FillRowMethodName = "FillHttpResult", TableDefinition = "[HttpResponseCode] int, [ResponseBody] varbinary(max), [ErrorBody] varbinary(max)")]
    [return: SqlFacet(MaxSize = -1)]
    public static IEnumerable HttpDelete(string url )
    {
        HttpResult result;

        try
        {
            using (var response = SharedETL.Extract.MakeWebRequest(Extract.HttpVerb.Delete, url, null,
                new Extract.WebRequestOptions() {Timeout = 2000}, true))
            {

                result = new HttpResult((int) response.HttpWebResponse.StatusCode,
                    new SqlBytes(UTF8Encoding.UTF8.GetBytes(response.Result)), SqlBytes.Null);
            }
        }
        catch (UnauthorizedAccessException uaex)
        {
            result = new HttpResult(403, SqlBytes.Null, new SqlBytes(UTF8Encoding.UTF8.GetBytes(uaex.ToString())));
        }
        catch (WebException wex)
        {
            result = new HttpResult(500, SqlBytes.Null, new SqlBytes(UTF8Encoding.UTF8.GetBytes(wex.Message)));
        }
        catch (Exception ex)
        {
            result = new HttpResult(500, SqlBytes.Null, new SqlBytes(UTF8Encoding.UTF8.GetBytes(ex.ToString())));
        }
        yield return result;
    }

    [SqlFunction(IsDeterministic = false, FillRowMethodName = "FillHttpResult", TableDefinition = "[HttpResponseCode] int, [ResponseBody] varbinary(max), [ErrorBody] varbinary(max)")]
    [return: SqlFacet(MaxSize = -1)]
    public static IEnumerable HttpPut(string url , byte[] contentBytes)
    {
        HttpResult result;
        string content = contentBytes == null || contentBytes.Length == 0 ? null : UnicodeEncoding.Unicode.GetString(contentBytes);

        try
        {
            using (
                var response = SharedETL.Extract.MakeWebRequest(Extract.HttpVerb.Put, url,
                    new Extract.WebRequestContent(content, SharedETL.Extract.ContentType, null),
                    new Extract.WebRequestOptions() {Timeout = 2000}, true))
            {

                result = new HttpResult((int) response.HttpWebResponse.StatusCode,
                    new SqlBytes(UTF8Encoding.UTF8.GetBytes(response.Result)), SqlBytes.Null);
            }
        }
        catch (UnauthorizedAccessException uaex)
        {
            result = new HttpResult(403, SqlBytes.Null, new SqlBytes(UTF8Encoding.UTF8.GetBytes(uaex.ToString())));
        }
        catch (WebException wex)
        {
            result = new HttpResult(500, SqlBytes.Null, new SqlBytes(UTF8Encoding.UTF8.GetBytes(wex.Message)));
        }
        catch (Exception ex)
        {
            result = new HttpResult(500, SqlBytes.Null, new SqlBytes(UTF8Encoding.UTF8.GetBytes(ex.ToString())));
        }
        yield return result;
    }

    [SqlFunction(IsDeterministic = false, FillRowMethodName = "FillHttpResult", TableDefinition = "[HttpResponseCode] int, [ResponseBody] varbinary(max), [ErrorBody] varbinary(max)")]
    [return: SqlFacet(MaxSize = -1)]
    public static IEnumerable HttpPost(string url, byte[]  contentBytes)
    {
        HttpResult result;
        string content = contentBytes == null || contentBytes.Length == 0 ? null : UnicodeEncoding.Unicode.GetString(contentBytes);

        try
        {
            using (
                var response = SharedETL.Extract.MakeWebRequest(Extract.HttpVerb.Post, url,
                    new Extract.WebRequestContent(content, SharedETL.Extract.ContentType, null),
                    new Extract.WebRequestOptions() {Timeout = 2000}, true))
            {

                result = new HttpResult((int) response.HttpWebResponse.StatusCode,
                    new SqlBytes(UTF8Encoding.UTF8.GetBytes(response.Result)), SqlBytes.Null);
            }
        }
        catch (UnauthorizedAccessException uaex)
        {
            result = new HttpResult(403, SqlBytes.Null, new SqlBytes(UTF8Encoding.UTF8.GetBytes(uaex.ToString())));
        }
        catch (WebException wex)
        {
            result = new HttpResult(500, SqlBytes.Null, new SqlBytes(UTF8Encoding.UTF8.GetBytes(wex.Message)));
        }
        catch (Exception ex)
        {
            result = new HttpResult(500, SqlBytes.Null, new SqlBytes(UTF8Encoding.UTF8.GetBytes(ex.ToString())));
        }
        yield return result;
    }

    [SqlFunction(IsDeterministic = false, FillRowMethodName = "FillHttpResult", TableDefinition = "[HttpResponseCode] int, [ResponseBody] varbinary(max), [ErrorBody] varbinary(max)")]
    [return: SqlFacet(MaxSize = -1)]
    public static IEnumerable HttpGet(string url )
    {
        HttpResult result;

        try
        {
            using (var response = SharedETL.Extract.MakeWebRequest(Extract.HttpVerb.Get, url, null,
                new Extract.WebRequestOptions() {Timeout = 2000}, true))
            {

                result = new HttpResult((int) response.HttpWebResponse.StatusCode,
                    new SqlBytes(UTF8Encoding.UTF8.GetBytes(response.Result)), SqlBytes.Null);
            }
        }
        catch (UnauthorizedAccessException uaex)
        {
            result = new HttpResult(403, SqlBytes.Null, new SqlBytes(UTF8Encoding.UTF8.GetBytes(uaex.ToString())));
        }
        catch (WebException wex)
        {
            result = new HttpResult(500, SqlBytes.Null, new SqlBytes(UTF8Encoding.UTF8.GetBytes(wex.Message)));
        }
        catch (Exception ex)
        {
            result = new HttpResult(500, SqlBytes.Null, new SqlBytes(UTF8Encoding.UTF8.GetBytes(ex.ToString())));
        }
        yield return result;
    }

    [SqlFunction(IsDeterministic =true,IsPrecise = false)]
    [return: SqlFacet(MaxSize =-1)]
    public static string UriEncode([SqlFacet(MaxSize =-1)]string text)
    {
        return Uri.EscapeUriString(text);
    }

    [SqlFunction(IsDeterministic = true, IsPrecise = false)]
    public static double LevenshteinScore([SqlFacet(MaxSize = -1)]string s1, [SqlFacet(MaxSize = -1)]string s2)
    {
        return LevenshteinDistance.DistanceScore(s1, s2);
    }

    [SqlFunction(IsDeterministic = true, IsPrecise = false)]
    public static double JaroWinklerScore([SqlFacet(MaxSize = -1)]string s1, [SqlFacet(MaxSize = -1)]string s2)
    {
        return JaroWinkler.GetJaroWinklerScore(s1, s2);
    }

    [SqlFunction(IsDeterministic = true,IsPrecise = false)]
    [return: SqlFacet(MaxSize = -1)]
    public static string RegExpJsonTrim([SqlFacet(MaxSize = -1)]string text, bool normalize, bool surroundWithQuotes, bool escapeJavascript)
    {
        string result = null;
        if (normalize == true && !String.IsNullOrEmpty(text))
        {
            result = text.NormalizeString();
            //result = normalizeSqlString.Replace(text.ToLower(), "");
        }
        else if (normalize == false && !String.IsNullOrEmpty(text))
        {
            result = removeInvalidCharacters.Replace(text, "");
        }

        if (escapeJavascript == true && !String.IsNullOrEmpty(result))
        {
            result = escapeJavascriptString.Replace(result,"\\$1");
        }

        if (surroundWithQuotes == true)
        {
            if (String.IsNullOrEmpty(result))
                result = "null";
            else 
                result = String.Concat("\"",result,"\"");
        }

        if (!String.IsNullOrEmpty(result))
        {
            result = result.Trim();
        }

        return result;
    }

    [SqlFunction(FillRowMethodName = "FillMatch", TableDefinition = "[Index] int, [Length] int, [Value] nvarchar(255)")]
    public static IEnumerable RegExpMatches(string text, string pattern, bool ignoreCase)
    {
        RegexOptions reg_ex_opt;

        if (ignoreCase == true)
        {
            reg_ex_opt = RegexOptions.IgnoreCase | RegexOptions.CultureInvariant;
        }
        else
        {
            reg_ex_opt = RegexOptions.None;
        }

        var result = Regex.Matches(text, pattern, reg_ex_opt);
        var enumerate = result.GetEnumerator();
        while(enumerate.MoveNext())
        {
            var match = enumerate.Current as Match;
            yield return new MatchResult(match.Index, match.Length, match.Value);
        }
    }

    [SqlFunction(FillRowMethodName = "FillReplace", TableDefinition = "[Value] nvarchar(4000)")]
    public static IEnumerable RegExpReplacements(string text, string pattern, string replacement, bool ignoreCase)
    {
        RegexOptions reg_ex_opt;

        if (ignoreCase == true)
        {
            reg_ex_opt = RegexOptions.IgnoreCase;
        }
        else
        {
            reg_ex_opt = RegexOptions.None;
        }

        var result = Regex.Replace(text, pattern, replacement, reg_ex_opt);
        yield return new ReplaceResult(result);
    }

    public static void FillMatch(object obj, out SqlInt32 index, out SqlInt32 length, out SqlString value)
    {
        MatchResult match = (MatchResult)obj;
        index = match.Index;
        length = match.Length;
        value = match.Value;
    }

    public static void FillHttpResult(object obj, out SqlInt32 httpResponseCode, out SqlBytes responseBody,
        out SqlBytes errorBody)
    {
        HttpResult httpResult = (HttpResult) obj;
        httpResponseCode = httpResult.HttpResponseCode;
        responseBody = httpResult.ResponseBody;
        errorBody = httpResult.ErrorBody;
    }

    public static void FillReplace(object obj, out SqlString value)
    {
        ReplaceResult replace = (ReplaceResult)obj;
        value = replace.Value;
    }


}
