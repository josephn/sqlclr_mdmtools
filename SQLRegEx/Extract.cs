﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace SharedETL
{
    /// <summary>
    /// Do not add anything here that can't be used in SQLCLR. This means C style procedural programming and external references.
    /// This class is for extracting data to other systems.
    /// </summary>
    public static class Extract
    {
        [Serializable]
        public class WebRequestOptions
        {
            //public WebRequestReplayInfo Replay { get; set; }
            /// <summary>
            /// The number of milliseconds to wait before the request times out. The default value is 100,000 milliseconds (100 seconds).
            /// </summary>
            public int Timeout { get; set; }
            public bool KeepAlive { get; set; }
            public string UserAgent { get; set; }
            //public HttpCookie[] Cookies { get; set; }
            public string Accept { get; set; }

            public WebRequestOptions()
            {
                //Cookies = new HttpCookie[0];
                Timeout = 100000;
            }
        }

        [Serializable]
        public class WebRequestHeader
        {
            public string Name { get; set; }
            public string Value { get; set; }
        }

        [Serializable]
        public class WebRequestContent
        {
            public List<WebRequestHeader> RequestHeaders { get; set; }
            public string ContentType { get; set; }
            public string DataToSend { get; set; }

            /// <summary>
            /// Encoding.  Default it UTF8Encoding.
            /// </summary>
            [XmlIgnore]
            public Encoding Encoding { get; set; }

            public WebRequestContent()
            {
                RequestHeaders = RequestHeaders ?? new List<WebRequestHeader>();
                Encoding = new UTF8Encoding();
            }

            public WebRequestContent(string dataToSend, string contentType, List<WebRequestHeader> requestHeaders = null)
                : this()
            {
                DataToSend = dataToSend;
                ContentType = contentType;
                RequestHeaders = requestHeaders ?? new List<WebRequestHeader>();
            }
        }
        public static string UriCombine(string basePath, string relativePath, bool includeEndingSlash = true)
        {
            if (string.IsNullOrEmpty(basePath) && string.IsNullOrEmpty(relativePath))
                return string.Empty;
            if (string.IsNullOrEmpty(basePath))
                return relativePath;
            if (string.IsNullOrEmpty(relativePath))
                return basePath;
            if (relativePath.StartsWith("~"))
                relativePath = relativePath.Substring(1);
            if (relativePath.StartsWith("/"))
                relativePath = relativePath.Substring(1);
            basePath = basePath.Trim();
            var path = basePath + (basePath.EndsWith("/") ? "" : "/") + relativePath.Trim();
            if (!includeEndingSlash && path.EndsWith("/"))
                path = path.Substring(0, path.Length - 1);
            return path;
        }
        public static ApiProxyApiResponse MakeWebRequest(HttpVerb httpVerb,
             string url,
            //IApiProxySettings settings,
            //bool logData = true,
             WebRequestContent content = null,
             WebRequestOptions options = null, bool anonymousCall = true)
        {

            //1. Check for null parameter (httpVerb, url, settings) on this method, and should directly throw an exception.
            if (string.IsNullOrEmpty(url))
            {
                throw new ApplicationException("ProxyUtilities::MakeWebRequest - url cannot be null.");
            }

            //if (settings == null)
            //{
            //    throw new ApplicationException("ProxyUtilities::MakeWebRequest - settings cannot be null");
            //}

            var verb = httpVerb.ToString().ToUpper();
            if (content == null)
                content = new WebRequestContent();
            if (options == null)
                options = new WebRequestOptions();

            //var stopwatch = new Stopwatch();
            HttpWebResponse response = null;
            string result = null;
            //try
            //{
            //settings.Logger.Debug(String.Format("Attempting HTTP " + verb + " To Url {0} With Data:\r\n\r\n {1}\r\n", url,
            //    content.DataToSend));
            if (httpVerb == HttpVerb.Get && !string.IsNullOrEmpty(content.DataToSend))
                url = UriCombine(url, content.DataToSend, false);
            var actualcontent = httpVerb == HttpVerb.Get || httpVerb == HttpVerb.Delete ? null : content.DataToSend;

            var theRequest = (HttpWebRequest)WebRequest.Create(url);
            theRequest.Method = verb;
            
            theRequest.ContentType = content.ContentType; // "text/x-json";
            theRequest.ContentLength = actualcontent == null ? 0 : actualcontent.Length;
            theRequest.Timeout = options.Timeout;
            theRequest.KeepAlive = options.KeepAlive;


            //var authorizationHeaderValue = (anonymousCall == true) ? GetSimpleWebToken(settings, ProxyUtilities.AnonymousClaimsPrincipal(settings)).AuthorizationHeaderValue() : GetSimpleWebToken(settings).AuthorizationHeaderValue();

            //theRequest.Headers[HttpRequestHeader.Authorization] = authorizationHeaderValue;

            //ServicePointManager
            //    .ServerCertificateValidationCallback +=
            //    (sender, cert, chain, sslPolicyErrors) => true;

            if (!string.IsNullOrEmpty(options.UserAgent))
                theRequest.UserAgent = options.UserAgent;

            //stopwatch.Start();

            foreach (var kvp in content.RequestHeaders.Where(kvp => theRequest.Headers[kvp.Name] == null))
            {
                theRequest.Headers.Add(kvp.Name, kvp.Value);
            }



            if (theRequest.Headers[HttpRequestHeader.Accept] == null || String.IsNullOrWhiteSpace(theRequest.Accept))
            {
                theRequest.Accept = ContentType;
            }

            if (actualcontent != null)
            {
                var bytes = content.Encoding.GetBytes(actualcontent);
                long byteCount = content.Encoding.GetByteCount(actualcontent);
                theRequest.ContentLength = byteCount;
                using (var requestStream = theRequest.GetRequestStream())
                {
                    requestStream.Write(bytes, 0, bytes.Length);
                    requestStream.Close();
                }
            }
            try
            {
                try
                {

                    response = (HttpWebResponse) theRequest.GetResponse();

                    if (response.StatusCode == HttpStatusCode.Unauthorized)
                    {
                        throw new UnauthorizedAccessException(
                            string.Format(
                                "The server at '{0}' denied the request.",
                                url));
                    }
                }
                catch (WebException wex)
                {
                    //  On line 226, check for null "response" variable first before using.
                    response = wex.Response as HttpWebResponse;
                    string exresult = String.Empty;

                    if (response != null)
                    {
                        using (var responseStream = response.GetResponseStream())
                        {
                            if (responseStream != null)
                                using (var readStream = new StreamReader(responseStream, Encoding.UTF8))
                                {
                                    exresult = readStream.ReadToEnd();
                                }
                        }

                        return new ApiProxyApiResponse() {HttpWebResponse = response, Result = exresult};

                        if (response.StatusCode == HttpStatusCode.Unauthorized)
                        {
                            var sb = new StringBuilder();
                            //if (settings.SessionProvider.ClaimsPrincipal != null)
                            //{
                            //    foreach (var claimDemand in _clientClaimDemands)
                            //    {
                            //        var claim =
                            //            settings.SessionProvider.ClaimsPrincipal.Claims.FirstOrDefault(
                            //                c => c.Type == claimDemand);
                            //        if (claim == null)
                            //            sb.AppendFormat("{0}=(no claim)", claimDemand);
                            //        else
                            //            sb.AppendFormat("{0}={1}", claimDemand, claim.Value);
                            //    }
                            //}

                            throw new UnauthorizedAccessException(
                                string.Format(
                                    "The server at '{0}' denied the request. {1}",
                                    url, exresult), wex);
                        }

                    }






                    //Debug.WriteLine(exresult);
                    throw new ApplicationException("The API server encountered an error: " + exresult, wex);

                }
                

                using (var responseStream = response.GetResponseStream())
                {
                    if (responseStream != null)
                        using (var readStream = new StreamReader(responseStream, Encoding.UTF8))
                        {
                            result = readStream.ReadToEnd();
                        }
                }
            }
            finally
            {
                
            }


            //if (logData)
            //    settings.Logger.Debug("HTTP " + verb + "; url: " + url + " Response: \r\n\r\n" + result + "\r\n");
            var resp = new ApiProxyApiResponse { HttpWebResponse = response, Result = result };

            return resp;
            //}
            //catch (Exception ex)
            //{
            //    settings.Logger.Error("Failed to " + verb + " Data." + "url: " + url, ex);

            //    throw;
            //}
            //finally
            //{
            //    if (stopwatch.IsRunning)
            //        stopwatch.Stop();
            //    var statusCode = response != null ? response.StatusCode.ToString() : "response is null.";
            //    settings.Logger.Performance(String.Format(
            //        "Time Elapsed: {0} to {1} Data To {2} Response Code: {3} Data: {4}\r\n", stopwatch.Elapsed, verb, url,
            //        statusCode,
            //        (content == null ? "(null)" : logData ? content.DataToSend : "<not logged>")));
            //}
        }

        public static HttpWebResponse GetHttpWebResponse(
            string taskName,
            //IApiProxyLogger logger,
            HttpWebRequestCallbackState callback,
            string verb,
            out string result)
        {
            if (callback.Exception == null)
            {
                // success
                HttpWebResponse response;
                try
                {
                    response = (HttpWebResponse)callback.WebResponse;
                }
                catch (WebException wex)
                {
                    response = wex.Response as HttpWebResponse;
                }

                using (var readStream = new StreamReader(callback.ResponseStream, Encoding.UTF8))
                {
                    result = readStream.ReadToEnd();
                }
                return response;
            }

            //var message = string.Format("{0} - Failed to {1} Data.", taskName, verb);
            //logger.Error(message, callback.Exception);
            result = null;
            return null;
        }

        public class ApiProxyApiResponse:IDisposable
        {
            public HttpWebResponse HttpWebResponse { get; set; }

            public string Result { get; set; }
            public void Dispose()
            {
                try
                {
                    if (this.HttpWebResponse != null)
                        this.HttpWebResponse.Dispose();
                }
                catch
                {
                }
            }
        }

        public enum HttpVerb
        {
            Get,
            Post,
            Delete,
            Put,
            Connect,
            Head,
            Trace
        }

        public static class HttpSocket
        {
            private static HttpWebRequest CreateHttpWebRequest(string url, string httpMethod, string contentType,
                                                               WebRequestOptions options = null)
            {
                var httpWebRequest = (HttpWebRequest)WebRequest.Create(requestUriString: url);
                httpWebRequest.ContentType = contentType; // "text/x-json";
                httpWebRequest.Method = httpMethod;
                if (options != null)
                {
                    httpWebRequest.Timeout = options.Timeout;
                    httpWebRequest.KeepAlive = options.KeepAlive;
                }
                return httpWebRequest;
            }

            static byte[] GetRequestBytes(NameValueCollection postParameters)
            {
                if (postParameters == null || postParameters.Count == 0)
                {
                    return new byte[0];
                }
                var sb = new StringBuilder();
                foreach (var key in postParameters.AllKeys)
                {
                    sb.Append(value: key + "=" + postParameters[key] + "&");
                }
                sb.Length = sb.Length - 1;
                return Encoding.UTF8.GetBytes(s: sb.ToString());
            }

            static byte[] GetRequestBytes(string postBody)
            {
                return Encoding.UTF8.GetBytes(s: postBody);
            }

            static void BeginGetRequestStreamCallback(IAsyncResult asyncResult)
            {
                Stream requestStream = null;
                HttpWebRequestAsyncState asyncState = null;
                try
                {
                    asyncState = (HttpWebRequestAsyncState)asyncResult.AsyncState;
                    requestStream = asyncState.HttpWebRequest.EndGetRequestStream(asyncResult: asyncResult);
                    requestStream.Write(buffer: asyncState.RequestBytes, offset: 0, count: asyncState.RequestBytes.Length);
                    requestStream.Close();
                    asyncState.HttpWebRequest.BeginGetResponse(callback: BeginGetResponseCallback,
                                                               state: new HttpWebRequestAsyncState
                                                               {
                                                                   HttpWebRequest = asyncState.HttpWebRequest,
                                                                   ResponseCallback = asyncState.ResponseCallback,
                                                                   State = asyncState.State
                                                               });
                }
                catch (Exception ex)
                {
                    if (asyncState != null)
                    {
                        asyncState.ResponseCallback(new HttpWebRequestCallbackState(exception: ex));
                    }
                    else
                    {
                        throw;
                    }
                }
                finally
                {
                    if (requestStream != null)
                    {
                        requestStream.Close();
                    }
                }
            }

            static void BeginGetResponseCallback(IAsyncResult asyncResult)
            {
                WebResponse webResponse = null;
                Stream responseStream = null;
                HttpWebRequestAsyncState asyncState = null;
                try
                {
                    asyncState = (HttpWebRequestAsyncState)asyncResult.AsyncState;
                    webResponse = asyncState.HttpWebRequest.EndGetResponse(asyncResult: asyncResult);
                    responseStream = webResponse.GetResponseStream();
                    var webRequestCallbackState = new HttpWebRequestCallbackState(responseStream: responseStream, state: asyncState.State);
                    asyncState.ResponseCallback(webRequestCallbackState);
                    responseStream.Close();
                    responseStream = null;
                    webResponse.Close();
                    webResponse = null;
                }
                catch (Exception ex)
                {
                    if (asyncState != null)
                    {
                        asyncState.ResponseCallback(new HttpWebRequestCallbackState(exception: ex));
                    }
                    else
                    {
                        throw;
                    }
                }
                finally
                {
                    if (responseStream != null)
                    {
                        responseStream.Close();
                    }
                    if (webResponse != null)
                    {
                        webResponse.Close();
                    }
                }
            }

            /// <summary>
            /// If the response from a remote server is in text form
            /// you can use this method to get the text from the ResponseStream
            /// This method Disposes the stream before it returns
            /// </summary>
            /// <param name="responseStream">The responseStream that was provided in the callback delegate's HttpWebRequestCallbackState parameter</param>
            /// <returns></returns>
            public static string GetResponseText(Stream responseStream)
            {
                using (var reader = new StreamReader(stream: responseStream))
                {
                    return reader.ReadToEnd();
                }
            }

            /// <summary>
            /// This method does an Http POST sending any post parameters to the url provided
            /// </summary>
            /// <param name="url">The url to make an Http POST to</param>
            /// <param name="postParameters">The form parameters if any that need to be POSTed</param>
            /// <param name="responseCallback">The callback delegate that should be called when the response returns from the remote server</param>
            /// <param name="state">Any state information you need to pass along to be available in the callback method when it is called</param>
            /// <param name="contentType">The Content-Type of the Http request</param>
            public static void DeleteAsync(string url, NameValueCollection postParameters, Action<HttpWebRequestCallbackState> responseCallback, object state = null, string contentType = ContentType)
            {
                var httpWebRequest = CreateHttpWebRequest(url: url, httpMethod: "DELETE", contentType: contentType);
                var requestBytes = GetRequestBytes(postParameters: postParameters);
                httpWebRequest.ContentLength = requestBytes.Length;

                httpWebRequest.BeginGetRequestStream(callback: BeginGetRequestStreamCallback,
                                                     state: new HttpWebRequestAsyncState()
                                                     {
                                                         RequestBytes = requestBytes,
                                                         HttpWebRequest = httpWebRequest,
                                                         ResponseCallback = responseCallback,
                                                         State = state
                                                     });
            }

            /// <summary>
            /// This method does an Http POST sending any post parameters to the url provided
            /// </summary>
            /// <param name="url">The url to make an Http POST to</param>
            /// <param name="postParameters">The form parameters if any that need to be POSTed</param>
            /// <param name="responseCallback">The callback delegate that should be called when the response returns from the remote server</param>
            /// <param name="state">Any state information you need to pass along to be available in the callback method when it is called</param>
            /// <param name="contentType">The Content-Type of the Http request</param>
            public static void PutAsync(string url, NameValueCollection postParameters, Action<HttpWebRequestCallbackState> responseCallback, object state = null, string contentType = ContentType)
            {
                var httpWebRequest = CreateHttpWebRequest(url: url, httpMethod: "PUT", contentType: contentType);
                var requestBytes = GetRequestBytes(postParameters: postParameters);
                httpWebRequest.ContentLength = requestBytes.Length;

                httpWebRequest.BeginGetRequestStream(callback: BeginGetRequestStreamCallback,
                                                     state: new HttpWebRequestAsyncState()
                                                     {
                                                         RequestBytes = requestBytes,
                                                         HttpWebRequest = httpWebRequest,
                                                         ResponseCallback = responseCallback,
                                                         State = state
                                                     });
            }

            /// <summary>
            /// This method does an Http POST sending any post parameters to the url provided
            /// </summary>
            /// <param name="url">The url to make an Http POST to</param>
            /// <param name="postParameters">The form parameters if any that need to be POSTed</param>
            /// <param name="responseCallback">The callback delegate that should be called when the response returns from the remote server</param>
            /// <param name="state">Any state information you need to pass along to be available in the callback method when it is called</param>
            /// <param name="contentType">The Content-Type of the Http request</param>
            public static void PostAsync(string url, NameValueCollection postParameters, Action<HttpWebRequestCallbackState> responseCallback, object state = null, string contentType = ContentType)
            {
                var httpWebRequest = CreateHttpWebRequest(url: url, httpMethod: "POST", contentType: contentType);
                var requestBytes = GetRequestBytes(postParameters: postParameters);
                httpWebRequest.ContentLength = requestBytes.Length;

                httpWebRequest.BeginGetRequestStream(callback: BeginGetRequestStreamCallback,
                                                     state: new HttpWebRequestAsyncState()
                                                     {
                                                         RequestBytes = requestBytes,
                                                         HttpWebRequest = httpWebRequest,
                                                         ResponseCallback = responseCallback,
                                                         State = state
                                                     });
            }

            public static void PostAsync(string url, string postBody, Action<HttpWebRequestCallbackState> responseCallback, NetworkCredential credential = null, object state = null, string contentType = ContentType)
            {
                var httpWebRequest = CreateHttpWebRequest(url: url, httpMethod: "POST", contentType: contentType);
                httpWebRequest.Credentials = credential;
                httpWebRequest.ContentLength = Encoding.UTF8.GetByteCount(s: postBody);

                var requestBytes = GetRequestBytes(postBody: postBody);
                httpWebRequest.ContentLength = requestBytes.Length;

                httpWebRequest.BeginGetRequestStream(callback: BeginGetRequestStreamCallback,
                                                     state: new HttpWebRequestAsyncState()
                                                     {
                                                         RequestBytes = requestBytes,
                                                         HttpWebRequest = httpWebRequest,
                                                         ResponseCallback = responseCallback,
                                                         State = state
                                                     });
            }

            /// <summary>
            /// This method does an Http GET to the provided url and calls the responseCallback delegate
            /// providing it with the response returned from the remote server.
            /// </summary>
            /// <param name="url">The url to make an Http GET to</param>
            /// <param name="responseCallback">The callback delegate that should be called when the response returns from the remote server</param>
            /// <param name="state">Any state information you need to pass along to be available in the callback method when it is called</param>
            /// <param name="contentType">The Content-Type of the Http request</param>
            public static void GetAsync(string url, Action<HttpWebRequestCallbackState> responseCallback, object state = null, string contentType = ContentType)
            {
                var httpWebRequest = CreateHttpWebRequest(url: url, httpMethod: "GET", contentType: contentType);

                httpWebRequest.BeginGetResponse(callback: BeginGetResponseCallback,
                                                state: new HttpWebRequestAsyncState()
                                                {
                                                    HttpWebRequest = httpWebRequest,
                                                    ResponseCallback = responseCallback,
                                                    State = state
                                                });
            }

            public static Task PostAsyncTask(string url, NameValueCollection postParameters, Action<HttpWebRequestCallbackState> responseCallback, object state = null, string contentType = ContentType)
            {
                return PostAsyncTask(url: url, requestBytes: GetRequestBytes(postParameters: postParameters), responseCallback: responseCallback, state: state, contentType: contentType);
            }

            public static Task PostAsyncTask(string url, string requestData, Action<HttpWebRequestCallbackState> responseCallback, object state = null, string contentType = ContentType)
            {
                return PostAsyncTask(url: url, requestBytes: GetRequestBytes(requestData), responseCallback: responseCallback, state: state, contentType: contentType);
            }

            public static Task PostAsyncTask(string url, byte[] requestBytes, Action<HttpWebRequestCallbackState> responseCallback, string authorizationHeaderValue = null, WebRequestContent Content = null, WebRequestOptions Options = null, object state = null, string contentType = ContentType)
            {
                var httpWebRequest = PrepareHttpWebRequest(
                                    httpMethod: "POST", url: url, authorizationHeaderValue: authorizationHeaderValue, Content: Content, Options: Options);
                httpWebRequest.ContentLength = requestBytes.Length;

                var asyncState = new HttpWebRequestAsyncState()
                {
                    RequestBytes = requestBytes,
                    HttpWebRequest = httpWebRequest,
                    ResponseCallback = responseCallback,
                    State = state
                };

                var postTask = Task.Factory.FromAsync<Stream>(beginMethod: httpWebRequest.BeginGetRequestStream,
                                                              endMethod: httpWebRequest.EndGetRequestStream,
                                                              state: asyncState,
                                                              creationOptions: TaskCreationOptions.None)
                                                              .ContinueWith<HttpWebRequestAsyncState>(continuationFunction: task =>
                                                              {
                                                                  var asyncState2 = (HttpWebRequestAsyncState)task.AsyncState;
                                                                  using (var requestStream = task.Result)
                                                                  {
                                                                      requestStream.Write(buffer: asyncState2.RequestBytes, offset: 0, count: asyncState2.RequestBytes.Length);
                                                                  }
                                                                  return asyncState2;
                                                              })
                    .ContinueWith(continuationAction: task =>
                    {
                        var httpWebRequestAsyncState2 = (HttpWebRequestAsyncState)task.Result;
                        var hwr2 = httpWebRequestAsyncState2.HttpWebRequest;
                        try
                        {
                            WebResponse webResponse = null;
                            Stream responseStream = null;
                            try
                            {
                                webResponse = hwr2.GetResponse();
                                responseStream = webResponse.GetResponseStream();
                                responseCallback(new HttpWebRequestCallbackState(responseStream: responseStream, state: httpWebRequestAsyncState2, webResponse: webResponse));
                            }
                            finally
                            {
                                if (responseStream != null)
                                {
                                    responseStream.Close();
                                }
                                if (webResponse != null)
                                {
                                    webResponse.Close();
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            responseCallback(new HttpWebRequestCallbackState(exception: ex));
                        }
                    });

                return postTask;
            }

            public static Task GetAsyncTask(string url, Action<HttpWebRequestCallbackState> responseCallback, string authorizationHeaderValue = null, WebRequestContent Content = null, WebRequestOptions Options = null, object state = null)
            {
                var httpWebRequest = PrepareHttpWebRequest(
                                    httpMethod: "GET", url: url, authorizationHeaderValue: authorizationHeaderValue, Content: Content, Options: Options);

                var getTask = Task.Factory.FromAsync<WebResponse>(beginMethod: httpWebRequest.BeginGetResponse,
                                                                  endMethod: httpWebRequest.EndGetResponse,
                                                                  state: null).ContinueWith(continuationAction: task =>
                                                                  {
                                                                      try
                                                                      {
                                                                          var webResponse = task.Result;
                                                                          var responseStream = webResponse.GetResponseStream();
                                                                          responseCallback(new HttpWebRequestCallbackState(responseStream: responseStream, state: state, webResponse: webResponse));
                                                                          responseStream.Close();
                                                                          webResponse.Close();
                                                                      }
                                                                      catch (Exception ex)
                                                                      {
                                                                          responseCallback(new HttpWebRequestCallbackState(exception: ex));
                                                                      }
                                                                  });
                return getTask;
            }

            private static HttpWebRequest PrepareHttpWebRequest(
                string httpMethod, string url, string authorizationHeaderValue, WebRequestContent Content, WebRequestOptions Options)
            {
                if (Content == null)
                {
                    Content = new WebRequestContent();
                }

                if (Options == null)
                {
                    Options = new WebRequestOptions();
                }

                var httpWebRequest = CreateHttpWebRequest(url, httpMethod, Content.ContentType, Options);

                httpWebRequest.Headers[HttpRequestHeader.Authorization] = authorizationHeaderValue;

                if (!string.IsNullOrEmpty(Options.UserAgent))
                {
                    httpWebRequest.UserAgent = Options.UserAgent;
                }

                foreach (var kvp in Content.RequestHeaders.Where(kvp => httpWebRequest.Headers[kvp.Name] == null))
                {
                    httpWebRequest.Headers.Add(name: kvp.Name, value: kvp.Value);
                }
                return httpWebRequest;
            }
        }

        /// <summary>
        /// This class is used to pass on "state" between each Begin/End call
        /// It also carries the user supplied "state" object all the way till
        /// the end where is then hands off the state object to the
        /// HttpWebRequestCallbackState object.
        /// </summary>
        public class HttpWebRequestAsyncState
        {
            public byte[] RequestBytes { get; set; }
            public HttpWebRequest HttpWebRequest { get; set; }
            public Action<HttpWebRequestCallbackState> ResponseCallback { get; set; }
            public Object State { get; set; }
        }

        /// <summary>
        /// This class is passed on to the user supplied callback method
        /// as a parameter. If there was an exception during the process
        /// then the Exception property will not be null and will hold
        /// a reference to the Exception that was raised.
        /// The ResponseStream property will be not null in the case of
        /// a sucessful request/response cycle. Use this stream to
        /// exctract the response.
        /// </summary>
        public class HttpWebRequestCallbackState
        {
            public WebResponse WebResponse { get; private set; }
            public Stream ResponseStream { get; private set; }
            public Exception Exception { get; private set; }
            public Object State { get; set; }

            public HttpWebRequestCallbackState(Stream responseStream, object state)
            {
                ResponseStream = responseStream;
                State = state;
            }

            public HttpWebRequestCallbackState(Stream responseStream, object state, WebResponse webResponse)
            {
                WebResponse = webResponse;
                ResponseStream = responseStream;
                State = state;
            }

            public HttpWebRequestCallbackState(Exception exception)
            {
                Exception = exception;
            }
        }
        public const string ContentType = "application/json";
    }
}
